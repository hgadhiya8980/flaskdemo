from datetime import datetime
import random

from flask import (flash, Flask, redirect, render_template, request,
                   session, url_for)
from pymongo import MongoClient
from flask_mail import Mail
import urllib.request
import os
from werkzeug.utils import secure_filename

app = Flask(__name__)

DEBUG = True
MAIN_DICT = {}
SECRET_KEY = 'Sm9obiBTY2hyb20ga2lja3MgYXNz'
# configuration of mail
app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'codescatter8980@gmail.com'
app.config['MAIL_PASSWORD'] = 'qrnvtobwftsippyd'
app.config['MAIL_USE_SSL'] = True
app.config['UPLOAD_FOLDER'] = '/static/uploads/'

mail = Mail(app)

secure_type = "http"

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif' 'svg'])

ALLOWED_EXTENSIONS_file = set(['pdf', 'doc'])

def allowed_photos(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_file

link_verificattion = ["https", "http", "www", "com", "org", "edu", "in"]

####database section

client = MongoClient("mongodb+srv://harshitgadhiya:Hgadhiya8980@codescatter.04ufqjh.mongodb.net/?retryWrites=true&w=majority")

db = client["resumebuilder"]

def register_data(coll_name, new_dict):
    try:
        coll = db[coll_name]
        coll.insert_one(new_dict)

        return "add_data"

    except Exception as e:
        print(e)

def find_all_cust_details():
    try:
        coll = db["customer_details"]
        res = coll.find({})
        return res

    except Exception as e:
        print(e)

def find_all_cust_details_coll(coll_name):
    try:
        coll = db[coll_name]
        res = coll.find({})
        return res

    except Exception as e:
        print(e)


def find_all_specific_user(coll_name, di):
    try:
        coll = db[coll_name]
        res = coll.find(di)
        return res

    except Exception as e:
        print(e)

def delete_data(coll_name, di):
    try:
        coll = db[coll_name]
        res = coll.delete_one(di)
        return res

    except Exception as e:
        print(e)

def update_data(coll_name, prev_data, update_data):
    try:
        coll = db[coll_name]
        coll.update_one(prev_data, update_data)
        return "updated"

    except Exception as e:
        print(e)

def checking_upload_folder(filename):
    try:
        entries = os.listdir(app.config["UPLOAD_FOLDER"])
        if filename in entries:
            return "duplicate"
        else:
            return "not duplicate"

    except Exception as e:
        print(e)


# That is route for register page and save customer data
@app.route("/register", methods=["GET", "POST"])
def register():
    """
    That function was register for new user
    """

    try:
        if request.method == "POST":
            name = request.form["name"]
            email = request.form["email"]
            phone = request.form["phone"]
            address = request.form["address"]
            username = request.form["username"]
            pwd = request.form["pwd"]
            re_pwd = request.form["re_pwd"]

            if "gmail" in email:
                pass
            else:
                flash("Please enter valid gmail ID..........")
                return redirect(url_for('register', _external=True, _scheme=secure_type))

            if " " not in username:
                pass
            else:
                flash("Space are not allow in username..........")
                return redirect(url_for('register', _external=True, _scheme=secure_type))

            if pwd == re_pwd:
                res = find_all_cust_details()
                username_list = [data.get("username", "") for data in res]
                if username in username_list:
                    flash("Username is already availble!!!! Please try with another username....")
                    return redirect(url_for('register', _external=True, _scheme=secure_type))
                else:
                    session["email"] = email
                    created_on = updated_on = datetime.now().strftime("%Y/%m/%d %H:%M")

                    new_dict = {"name": name, "email": email, "phone": int(phone), "address": address,
                                "username": username, "password": pwd, "created_on": created_on,
                                "updated_on": updated_on}
                    session["new_dict"] = new_dict

                    return redirect(url_for('verification', _external=True, _scheme=secure_type))
            else:
                flash("Password doesn't match!!")
                return render_template("auth/register.html")
        else:
            return render_template("auth/register.html")

    except Exception as e:
        flash("Please try again......................")
        return render_template("auth/register.html")


@app.route("/otp_sending_ver", methods=["GET", "POST"])
def otp_sending_ver():
    """
    That funcation was sending a otp for user
    """

    try:
        otp = random.randint(100000, 999999)
        session["otp"] = otp
        mail.send_message("OTP Received",
                          sender="harshitgadhiya8980@gmail.com",
                          recipients=[session.get("email", "")],
                          body='Hello Sir/madam \nYour OTP is {0}\nThis OTP is valid only 10 miniuts....'.format(otp))
        flash("OTP sending successfully...........")
        return redirect(url_for('verification', _external=True, _scheme=secure_type))

    except Exception as e:
        flash("Please try again.......................................")
        return redirect(url_for('verification', _external=True, _scheme=secure_type))


@app.route("/verification", methods=["GET", "POST"])
def verification():
    """
    That funcation can use otp_verification and new_password set link generate
    """

    try:
        if request.method == "POST":
            get_otp = request.form["otp"]
            get_otp = int(get_otp)
            send_otp = session.get("otp", "")
            if get_otp == int(send_otp):
                new_dict = session.get("new_dict", "")
                session["username"] = new_dict.get("username", "")
                new_dict1 = {"name": new_dict["name"], "email": new_dict['email'], "phone": int(new_dict['phone']),
                             "address": new_dict['address'], "username": new_dict['username'],
                             "password": new_dict['password'], "created_on": new_dict['created_on'],
                             "updated_on": new_dict['updated_on']}

                add_data = register_data(coll_name="customer_details", new_dict=new_dict1)

                mail.send_message("Successfully Register",
                                  sender="harshitgadhiya8980@gmail.com",
                                  recipients=["codescatter8980@gmail.com"],
                                  body="Hello 1 user added")
                flash("Successfully Register!!!...........")
                return redirect(url_for('dash_home', _external=True, _scheme=secure_type))
            else:
                flash("OTP is wrong. Please enter correct otp")
                return redirect(url_for('verification', _external=True, _scheme=secure_type))
        else:
            return render_template("auth/verification.html")

    except Exception as e:
        flash("Please try again.......................................")
        return redirect(url_for('verification', _external=True, _scheme=secure_type))


# That function should be login into that product
@app.route("/", methods=["GET", "POST"])
def login():
    """
    That route can use login user
    """

    try:
        if request.method == "POST":
            username = request.form["username"]
            pwd = request.form["pwd"]

            res = find_all_cust_details()
            username_list = [[data.get("username", ""), data.get("password", "")] for data in res]
            email_list = [[data.get("email", ""), data.get("password", "")] for data in res]

            if [username, pwd] in username_list or [username, pwd] in email_list:
                session["username"] = username
                flash("Successfully Login")
                return redirect(url_for('dash_home', _external=True, _scheme=secure_type))
            else:
                flash("Your credentials doesn't match! Please enter correct Username and password...")
                return render_template("auth/login.html")
        else:
            return render_template("auth/login.html")

    except Exception as e:
        flash("Please try again..............................")
        return render_template("auth/login.html")


@app.route("/privacy_policy", methods=["GET", "POST"])
def privacy_policy():
    """
    That route can use login user
    """

    try:
        return render_template("privacy_policy.html")

    except Exception as e:
        flash("Please try again..............................")
        return render_template("privacy_policy.html")


# That is route for sending forget mail for user
@app.route("/sending_forget_mail", methods=["GET", "POST"])
def sending_forget_mail():
    """
    That function was sending forget mail while user can forget password
    """

    try:
        username = session.get("username", "")

        if request.method == "POST":
            username = request.form["username"]
            email = request.form["email"]

            res = find_all_cust_details()
            username_list = [data.get("username", "") for data in res]
            email_list = [data.get("email", "") for data in res]
            if username in username_list or username in email_list:
                mail.send_message("Forget_Password",
                                  sender="harshitgadhiya8980@gmail.com",
                                  recipients=[email],
                                  body='Hello user\nChange Password\nclick that link https://codedresume.pythonanywhere.com/forget_password')
                flash("Pleas check your mail............")
                return redirect(url_for('sending_forget_mail', _external=True, _scheme=secure_type))
            else:
                flash("That {0} is not availble First you can register!!".format(username))
                return redirect(url_for('sending_forget_mail', _external=True, _scheme=secure_type))
        else:
            return render_template("auth/sending_forget_mail.html")

    except Exception as e:
        flash("Please try again.........................")
        return render_template("auth/sending_forget_mail.html")


# That is route for otp sending mail for user
@app.route("/otp_sending", methods=["GET", "POST"])
def otp_sending():
    """
    That funcation was sending a otp for user
    """

    try:
        otp = random.randint(100000, 999999)
        session["otp"] = otp
        mail.send_message("OTP Received",
                          sender="harshitgadhiya8980@gmail.com",
                          recipients=[session.get("email", "")],
                          body='Hello {0}\nYour OTP is {1}\nThis OTP is valid only 10 miniuts....'.format(
                              session["username"], otp))

        return redirect(url_for('forget_password', _external=True, _scheme=secure_type))

    except Exception as e:
        flash("Please try again.......................................")
        return redirect(url_for('forget_password', _external=True, _scheme=secure_type))


# That is route for otp verification and sending new_password created link
@app.route("/forget_password", methods=["GET", "POST"])
def forget_password():
    """
    That funcation can use otp_verification and new_password set link generate
    """

    try:
        if request.method == "POST":
            get_otp = request.form["otp"]
            get_otp = int(get_otp)
            send_otp = session.get("otp", "")
            if get_otp == int(send_otp):
                return redirect(url_for('change_password', _external=True, _scheme=secure_type))
            else:
                flash("OTP is wrong. Please enter correct otp")
                return redirect(url_for('forget_password', _external=True, _scheme=secure_type))
        else:
            return render_template("auth/forget_password.html")

    except Exception as e:
        flash("Please try again.......................................")
        return redirect(url_for('forget_password', _external=True, _scheme=secure_type))


# That is route for new_password generation
@app.route("/change_password", methods=["GET", "POST"])
def change_password():
    """
    That function was create a new password and update that data
    """
    try:
        if request.method == "POST":
            new_pwd = request.form["new_pwd"]
            re_new_pwd = request.form["re_new_pwd"]
            username = session.get("username", "")

            if new_pwd == re_new_pwd:
                prev_data = {"username": username}
                next_data = {'$set': {"password": new_pwd}}
                update_data(coll_name="customer_details", prev_data=prev_data, update_data=next_data)

                flash("Record was updated..................")
                return redirect(url_for('login', _external=True, _scheme=secure_type))
            else:
                flash("Password doesn't match!!")
                return redirect(url_for('change_password', _external=True, _scheme=secure_type))
        else:
            return render_template("auth/new_password.html")

    except Exception as e:
        flash("Please try again...................")
        return redirect(url_for('change_password', _external=True, _scheme=secure_type))


# That is logout route and clear the current session
@app.route('/logout/<key>', methods=['GET', 'POST'])
def logout(key):
    """
    That funcation was logout session and clear user session
    """

    try:
        session.clear()
        if key == "user":
            # clear the session when user logout
            return redirect(url_for('login', _external=True, _scheme=secure_type))
        else:
            return redirect(url_for('admin_login', _external=True, _scheme=secure_type))

    except Exception as e:
        flash("Please try again.......................................")
        return redirect(url_for('login', _external=True, _scheme=secure_type))

